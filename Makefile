PROJECT :=	cryptutils
VERSION :=	1.1.0

.PHONY: all
all:
	gb build all

debian:
	dh_make -p $(PROJECT)_$(VERSION) --createorig

.PHONY: debuild
debuild: debian
	dpkg-buildpackage -us -uc

.PHONY: install
install: all
	install -m 0755 bin/* $(DESTDIR)

.PHONY: clean
clean:
	rm -rf bin pkg


